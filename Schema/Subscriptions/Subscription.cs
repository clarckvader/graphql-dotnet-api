using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIGRAPHQLDEMO.Schema.Queries;
using APIGRAPHQLDEMO.Schema.Mutations;
using HotChocolate.Subscriptions;
using HotChocolate.Execution;

namespace APIGRAPHQLDEMO.Schema.Subscriptions
{
    public class Subscription
    {
        [Subscribe]
        public CourseResult CourseCreated([EventMessage] CourseResult course) => course;

        [SubscribeAndResolve]
        public ValueTask<ISourceStream<CourseResult>> courseUpdated(Guid courseId,
        [Service] ITopicEventReceiver topicEventReceiver){

            var topicName = $"{courseId}_{nameof(Subscription.CourseCreated)}";

            return topicEventReceiver.SubscribeAsync<CourseResult>(topicName);
        }
    }
}