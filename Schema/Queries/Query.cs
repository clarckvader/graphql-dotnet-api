using Bogus;

namespace APIGRAPHQLDEMO.Schema.Queries;

public class Query
{

        private readonly Faker<InstructorType> _instructorFaker;
        private readonly Faker<StudentType> _studentFaker;
        private readonly Faker<CourseType> _courseFaker;


        public Query(){
                _instructorFaker = new Faker<InstructorType>()
                .RuleFor(c => c.Id, f => Guid.NewGuid())
                .RuleFor(c => c.FirstName, f => f.Name.FirstName())
                .RuleFor(c => c.Lastname, f => f.Name.LastName())
                .RuleFor(c => c.Salary, f=> f.Random.Double(0, 10000));

                _studentFaker = new Faker<StudentType>()
                .RuleFor(c => c.Id, f => Guid.NewGuid())
                .RuleFor(c => c.FirstName, f => f.Name.FirstName())
                .RuleFor(c => c.Lastname, f => f.Name.LastName())
                .RuleFor(c => c.GPA, f => f.Random.Double(1, 4));


                _courseFaker = new Faker<CourseType>()
                .RuleFor(c => c.Id , f => Guid.NewGuid())
                .RuleFor(c => c.Name, f => f.Name.JobTitle())
                .RuleFor(c => c.Subject, f => f.PickRandom<Subject>())
                .RuleFor(c => c.students, f => _studentFaker.Generate(4))
                .RuleFor(c => c.Instructor, f => _instructorFaker.Generate());
                
        }
        public IEnumerable<CourseType> GetCourses()
        {       
                return _courseFaker.Generate(5);
        }

        public CourseType GetCourseById(Guid id){

        CourseType course = _courseFaker.Generate();

        course.Id = id;

        return course;
        }

        [GraphQLDeprecated("this query is deprecated")]
        public string saludo => "hola a todos";
}