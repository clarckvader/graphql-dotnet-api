namespace APIGRAPHQLDEMO.Schema.Queries
{
    public class PersonType
    {
        public Guid Id { get; set;}
        public string? FirstName { get; set;}
        public string? Lastname { get; set;}
    }
}