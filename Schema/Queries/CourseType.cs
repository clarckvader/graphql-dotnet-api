using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIGRAPHQLDEMO.Schema.Queries
{

    public enum Subject
    {
     Matematicas,
     Ciencias,
     Historia

    }
    public class CourseType
    {
        public Guid Id {get; set;}
        public string? Name { get; set;}

        public Subject Subject { get ; set; }

        [GraphQLNonNullType]
        public InstructorType? Instructor { get; set;}
        public IEnumerable<StudentType>? students { get; set;}
    }
}