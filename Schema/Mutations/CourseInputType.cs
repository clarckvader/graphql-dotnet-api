using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIGRAPHQLDEMO.Schema.Queries;

namespace APIGRAPHQLDEMO.Schema.Mutations
{
    public class CourseInputType
    {
        public string? name { get; set; }
        public Subject subject { get; set; }

        public Guid InstructorId { get; set; }

    }
}