using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIGRAPHQLDEMO.Schema.Queries;
using APIGRAPHQLDEMO.Schema.Subscriptions;
using HotChocolate.Subscriptions;

namespace APIGRAPHQLDEMO.Schema.Mutations
{
    public class Mutation
    {
        private readonly List<CourseResult> _courses;

        public Mutation(){
            _courses = new List<CourseResult>();
        }

        public async Task<CourseResult> createCourse(string name, Subject subject, Guid InstructorId, [Service] ITopicEventSender topicEventSender)
        {

            CourseResult courseType = new CourseResult()
            {
                Id = Guid.NewGuid(),
                Name = name,
                Subject = subject,
                InstructorId = InstructorId,
            };

            _courses.Add(courseType);

            await topicEventSender.SendAsync(nameof(Subscription.CourseCreated), courseType);

            return courseType;

        }

        public async Task<CourseResult> UpdateCourseById(Guid courseId, string name, Subject subject, Guid InstructorId, [Service] ITopicEventSender topicEventSender){

            CourseResult course =  _courses.FirstOrDefault(c => c.Id == courseId);

            if (course == null){
                throw new GraphQLException(new Error("Courser not found", "COURSE_NOT_FOUND"));
            }


            course.Name = name;
            course.Subject = subject;
            course.InstructorId = InstructorId;

            string updateCourseTopic = $"{course.Id}_{nameof(Subscription.CourseCreated)}";

            await topicEventSender.SendAsync(updateCourseTopic, course);

            return course;

        }


        public bool DeleteCourseById (Guid courseId){

            
            return _courses.RemoveAll(c => c.Id == courseId) >= 1;
        }
        
    }
}